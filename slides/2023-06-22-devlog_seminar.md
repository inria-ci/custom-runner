---
theme: gaia
_class: lead
paginate: true
backgroundColor: white
---

**custom-runner**

https://gitlab.inria.fr/inria-ci/custom-runner

---

# Une expérimentation pour les exécuteurs partagés du futur

Stage de deux mois d’Ihsane Merrir.

Des exécuteurs partagés pour l’intégration continue qui :

- utilisent les ressources de CloudStack,

- peuvent lancer des gabarits arbitraires (Linux, Mac OS, Windows),

- peuvent être dimensionnés pour la tâche à exécuter
  (nombre de cœurs, mémoire, disque).

---

# Limitations des exécuteurs partagés actuels

- Seulement Linux, seulement docker.

- Des ressources limitées sur un hyperviseur VirtualBox dédié.

- Des technologies périmées ou en passe de l’être : `docker+machine`,
  vieille version VirtualBox car nouvelle version incompatible,
  vieille version de CentOS...

---

# Spécifier la machine virtuelle

La clé `image` d’un travail décrit dans `.gitlab-ci.yml` permet:

- de spécifier le gabarit
```
test-ubuntu:
  stage: test
  tags: [custom.ci.inria.fr]
  image: featured:ubuntu-20.04-lts
  script:
    - echo Hello!
```

- d’éventuellement dimensionner la machine virtuelle,
  par exemple `featured:ubuntu-20.04-lts+cpu:8+ram:16+disk:100`.

---

# Expérimentation sous la forme d’un exécuteur non vérouillé

Il suffit de demander à être membre du projet

https://gitlab.inria.fr/inria-ci/custom-runner

pour pouvoir activer l’exécuteur dans ses propres projets (comme lors
de la phase de qualif des exécuteurs partagés actuels).

---

# Choix du projet CloudStack

Par défaut, les machines sont créées dans le projet `custom-runner`
(soumis aux quotas : 38 cœurs, 38912 Go de mémoire vive ;
ces quotas peuvent croître dans le futur).

Si on spécifie les variables CI/CD `CLOUDSTACK_API_KEY` et
`CLOUDSTACK_SECRET_KEY`, on peut cibler ses propres projets
(et bénéficier de ses quotas, ses gabarits de projet, etc.).

---

# Principe

- une machine virtuelle est créée à la volée pour chaque tâche
  (tant que le quota est insuffisant, on réessaye toutes les 10s)
  et est supprimée à la fin de l’exécution de la tâche,

- une fois la machine déployée, l’exécuteur s’y connecte par SSH
  (utilisateur `ci`, mot de passe `ci`, tant que la connexion échoue
  à cause du réseau, on réessaye toutes les 10s),

- enfin, on exécute le script `bash` donné par `gitlab-runner`,
  et ce-dernier gère l’exécution de la tâche elle-même
  (et les artéfacts).

---

# Qui peut le plus peut le moins : `ssh` seul pour accéder à des machines

```
test-cluster:
  tags: [custom.ci.inria.fr]
  image: ssh:ssh-pro.inria.fr:cleps.inria.fr
  script:
    - echo Hello!
```

L’exécuteur utilise la clé sans phrase de protection stockée en tant
que fichier dans la variable CI/CD `SSH_PRIVATE_KEY`.

---

# Quid de docker?

```
test-docker:
  stage: test
  tags: [custom.ci.inria.fr]
  image: community:ubuntu-22-04-docker+docker:alpine
  script:
    - uname -a
```

---

# Pistes à venir / à explorer

- GitLab propose un exécuteur `instances` pour maintenir un lot de
  machines virtuelles dimensionné en fonction de la demande, qui
  permettent ensuite d’exécuter très rapidement des dockers.
  À voir si on veut proposer ce service en plus
  (manque d'isolation ?).

- Préparer des gabarits avec des distributions plus petites
  (par exemple, alpine), pour être plus rapide.

- Permettre l'analyse post-mortem des machines virtuelles.
